<?php

use App\Http\Controllers\CourierController;
use App\Http\Controllers\CourierOrderController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('orders')->group(function () {
        Route::post('/{id}/delivered', [OrderController::class, 'delivered']);
        Route::apiResource('/', OrderController::class)->except(['update', 'destroy'])->parameter('', 'order');
    });

    Route::prefix('couriers')->group(function () {
        Route::get('/', [CourierController::class, 'index']);
        Route::get('/free', [CourierController::class, 'free']);
    });

    Route::apiResource('couriers.orders', CourierOrderController::class)->only(['index', 'store']);
});
