<?php

namespace Tests\Feature;

use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    public function testRequiredFields()
    {
        $response = $this->postJson('/api/v1/orders');
        $response
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "description" => [
                        "The description field is required."
                    ],
                    "destination" => [
                        "The destination field is required."
                    ],
                    "cost" => [
                        "The cost field is required."
                    ],
                    "admission_date" => [
                        "The admission date field is required."
                    ]
                ]
            ]);
    }

    public function testSuccessStore()
    {
        $order = Order::factory()->raw();

        $response = $this->postJson('/api/v1/orders', $order);
        $response
            ->assertStatus(200)
            ->assertJson(["message" => "Заказ создан"]);
        $this->assertDatabaseHas('orders', $order);
    }
}
