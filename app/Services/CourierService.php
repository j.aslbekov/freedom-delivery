<?php

namespace App\Services;

use App\Repositories\CourierRepository;

class CourierService extends BaseService
{
    private $courierRepository;

    public function __construct(CourierRepository $courierRepository)
    {
        $this->courierRepository = $courierRepository;
    }

    public function index()
    {
        return $this->result($this->courierRepository->index());
    }

    public function free()
    {
        return $this->result($this->courierRepository->free());
    }
}
