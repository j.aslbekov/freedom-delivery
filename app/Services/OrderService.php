<?php

namespace App\Services;

use App\Repositories\OrderRepository;
use Carbon\Carbon;

class OrderService extends BaseService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index()
    {
        return $this->result($this->orderRepository->index());
    }

    public function get($id)
    {
        $order = $this->orderRepository->get($id);

        if (is_null($order)) {
            return $this->errNotFound('Заказ не найден');
        }
        return $this->result($order);
    }

    public function store($data)
    {
        $this->orderRepository->store($data);

        return $this->ok('Заказ создан');
    }

    public function delivered($id)
    {
        $order = $this->orderRepository->get($id);

        if (is_null($order)) {
            return $this->errNotFound('Заказ не найден');
        }

        $order->delivered_date = Carbon::now();
        $order->save();

        return $this->ok('Статус заказа обнавлен');
    }
}
