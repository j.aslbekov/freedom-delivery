<?php

namespace App\Services;

use App\Repositories\CourierRepository;
use App\Repositories\OrderRepository;

class CourierOrderService extends BaseService
{
    private $courierRepository;
    private $orderRepository;

    public function __construct(CourierRepository $courierRepository, OrderRepository $orderRepository)
    {
        $this->courierRepository = $courierRepository;
        $this->orderRepository = $orderRepository;
    }

    public function index($courierId)
    {
        $courier = $this->courierRepository->get($courierId);
        if (is_null($courier)) {
            return $this->errNotFound('Курьер не найден');
        }

        return $this->result($this->courierRepository->getDeliveredOrders($courier));
    }

    public function store($courierId, $orderId)
    {
        $courier = $this->courierRepository->get($courierId);
        if (is_null($courier)) {
            return $this->errNotFound('Курьер не найден');
        }

        if (!is_null($this->courierRepository->getActiveOrder($courier))) {
            return $this->errValidate('Курьер занят другим заказом');
        }

        $order = $this->orderRepository->get($orderId);
        $courier->orders()->save($order);

        return $this->ok('Заказ закреплен за курьером');
    }

}
