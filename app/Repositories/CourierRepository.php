<?php

namespace App\Repositories;

use App\Models\Courier;
use Illuminate\Database\Eloquent\Builder;;

class CourierRepository
{
    public function index()
    {
        return Courier::all();
    }

    public function get(int $id): ?Courier
    {
        return Courier::find($id);
    }

    public function getDeliveredOrders(Courier $courier)
    {
        return $courier->orders()->whereNotNull('delivered_date')->get();
    }

    public function getActiveOrder(Courier $courier)
    {
        return $courier->orders()->whereNull('delivered_date')->first();
    }

    public function free()
    {
        return Courier::whereHas('orders', function (Builder $query) {
            $query->whereNotNull('delivered_date');
        })->get();
    }
}
