<?php

namespace App\Repositories;

use App\Models\Order;

class OrderRepository
{
    public function index()
    {
        return Order::all();
    }

    public function get(int $id): ?Order
    {
        return Order::find($id);
    }

    public function store($data): Order
    {
        return Order::create($data);
    }
}
