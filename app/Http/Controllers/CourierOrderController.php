<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourierOrderRequest;
use App\Http\Resources\OrderResource;
use App\Services\CourierOrderService;
use Illuminate\Http\Request;

class CourierOrderController extends Controller
{
    private $courierOrderService;

    public function __construct(CourierOrderService $courierOrderService)
    {
        $this->courierOrderService = $courierOrderService;
    }

    public function index($courierId)
    {
        $result = $this->courierOrderService->index($courierId);
        return $this->resultCollection(OrderResource::class, $result);
    }

    public function store(CourierOrderRequest $request, $courierId)
    {
        $data = $request->validated();
        return $this->result($this->courierOrderService->store($courierId, $data['order_id']));
    }
}
