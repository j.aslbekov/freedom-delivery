<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function index()
    {
        $result = $this->orderService->index();
        return $this->resultCollection(OrderResource::class, $result);
    }

    public function store(OrderRequest $request)
    {
        return $this->result($this->orderService->store($request->validated()));
    }

    public function show($id)
    {
        $result = $this->orderService->get($id);
        return $this->resultResource(OrderResource::class, $result);
    }

    public function delivered($id)
    {
        return $this->result($this->orderService->delivered($id));
    }
}
