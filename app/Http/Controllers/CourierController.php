<?php

namespace App\Http\Controllers;

use App\Http\Resources\CourierResource;
use App\Http\Resources\OrderResource;
use App\Models\Courier;
use App\Services\CourierService;
use Illuminate\Http\Request;

class CourierController extends Controller
{
    private $courierService;

    public function __construct(CourierService $courierService)
    {
        $this->courierService = $courierService;
    }

    public function index()
    {
        $result = $this->courierService->index();
        return $this->resultCollection(CourierResource::class, $result);
    }

    public function free()
    {
        $result = $this->courierService->free();
        return $this->resultCollection(CourierResource::class, $result);
    }
}
