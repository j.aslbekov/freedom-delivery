<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'destination' => $this->destination,
            'cost' => $this->cost,
            'admission_date' => $this->admission_date,
            'delivered_date' => $this->delivered_date,
            'courier' => new CourierResource($this->courier)
        ];
    }
}
