<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->paragraph(1),
            'destination' => $this->faker->address(),
            'cost' => $this->faker->randomFloat(null, 100, 10000),
            'admission_date' => $this->faker->dateTimeBetween('-3 years')
        ];
    }
}
